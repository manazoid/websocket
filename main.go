package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/google/uuid"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var (
	wsupgrader = websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	pool map[string]*PlayerSession
	last string
)

type PlayerSession struct {
	Players    [2]*websocket.Conn
	Playground PlayMatrix
}

type PlayMatrix struct {
	ID     string    `json:"id"`
	Field  [3][3]int `json:"field"`
	Result bool      `json:"result"`
}

type PlayerInput struct {
	X      int `json:"x"`
	Y      int `json:"y"`
	Player int `json:"player"`
}

func main() {
	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	pool = make(map[string]*PlayerSession)

	r.GET("/ws", handler)
	r.LoadHTMLFiles("index.html")
	r.GET("/", func(c *gin.Context) {
		c.HTML(200, "index.html", nil)
	})

	r.Run()
}

func handler(c *gin.Context) {
	wshandler(c.Writer, c.Request)
}

func genNew(conn *websocket.Conn) string {
	id := uuid.New().String()
	//id := fmt.Sprintf(`%d`, len(pool))

	pool[id] = &PlayerSession{}
	pool[id].Players[0] = conn

	return id
}

func connectExists(id string, conn *websocket.Conn) {
	pool[id].Players[1] = conn
}

// start connection
func wshandler(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(fmt.Sprintf("Failed to set websocket upgrade: %v", err))
		return
	}

	var number string

	if last == "" {
		number = genNew(conn)
		last = number
	} else {
		number = last
		last = ""
		connectExists(number, conn)
	}

	pool[number].Playground.ID = number
	//f := *pool[number]
	//fmt.Println(fmt.Sprintf(`%v:%s`, f, number))

	defer func() {
		for _, e := range pool[number].Players {
			if e != nil {
				e.Close()
			}
		}

		// TODO leaderboard

		delete(pool, number)
	}()

	sendJSON(conn, &pool[number].Playground, number)

	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}

		var input PlayerInput
		if err := json.Unmarshal(msg, &input); err != nil {
			log.Println(err)
		}

		if input.Player != 0 && input.X < 3 && input.Y < 3 {
			f := &pool[number].Playground.Field[input.X][input.Y]
			if *f == 0 {
				*f = input.Player
			} else {
				message := fmt.Sprintf(`{"panic": %d}`, *f)
				if err := sendCurrentUser(conn, []byte(message)); err != nil {
					log.Println(err)
				}
				continue
			}
		} else {
			fmt.Println("ERROR", input)
		}

		for _, e := range &pool[number].Playground.Field {
			for _, k := range e {
				fmt.Print(k)
			}
			fmt.Println()
		}

		sendJSON(nil, &pool[number].Playground, number)
	}
}

func sendJSON(conn *websocket.Conn, play *PlayMatrix, number string) {
	output, _ := json.Marshal(play)
	if conn != nil {
		if err := sendCurrentUser(conn, output); err != nil {
			log.Println("sendJSON", err)
		}
	} else {
		sendMessage(output, number)
	}
}

func sendCurrentUser(user *websocket.Conn, msg []byte) error {
	return user.WriteMessage(websocket.TextMessage, msg)
}

func sendMessage(msg []byte, id string) {
	fmt.Println(fmt.Sprintf(`%v`, pool[id].Players))
	for _, e := range pool[id].Players {
		if err := sendCurrentUser(e, msg); err != nil {
			continue
		}
	}
}
